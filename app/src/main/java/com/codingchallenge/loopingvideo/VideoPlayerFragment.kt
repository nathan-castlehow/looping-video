package com.codingchallenge.loopingvideo

// Exo Player
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File


/**
 * A simple [Fragment] subclass containing a video player as the default destination in the navigation.
 */
class VideoPlayerFragment : Fragment() {

    private var sharedPreferences: SharedPreferences? = null
    private var currentPlayer: SimpleExoPlayer? = null
    private val defaultSourceURI: String = "https://content.jwplatform.com/manifests/ZaLw9vYv.m3u8"

    private var preferenceChangeListener :OnSharedPreferenceChangeListener? = null

    // Keys to use for accessing preferences
    private val SHOULD_LOOP_PREF_KEY: String = "shouldLoopVideo"
    private val VIDEO_SOURCE_PREF_KEY: String = "videoSource"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialiseVideoPlayer()
        registerPreferenceUpdateHandler()
    }

    private fun initialiseVideoPlayer() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        
        val currentSourceURI = sharedPreferences!!.getString(VIDEO_SOURCE_PREF_KEY, defaultSourceURI)!!
        val currentSource: HlsMediaSource = getCacheBasedHlsMediaSource(currentSourceURI)

        // player setup
        currentPlayer = SimpleExoPlayer.Builder(requireContext()).build()
        currentPlayer!!.setMediaSource(currentSource)

        // loop setting
        val shouldLoop = sharedPreferences!!.getBoolean(SHOULD_LOOP_PREF_KEY, true)
        currentPlayer!!.repeatMode = if(shouldLoop) Player.REPEAT_MODE_ONE else Player.REPEAT_MODE_OFF


        //connect player to view
        val currentPlayerView: PlayerView =  requireView().findViewById<PlayerView>(R.id.exo_player_view)
        currentPlayerView.player = currentPlayer

        //player run
        currentPlayer!!.prepare()
        currentPlayer!!.play()

    }

    private fun getCacheBasedHlsMediaSource(sourceUrI: String): HlsMediaSource {
        // Create a data source factory.
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(requireContext())

        // cache factory
        val currentFactory: CacheDataSource.Factory = CacheDataSource.Factory().setCache(
            SimpleCacheSingleton.instance.getSimpleCache(requireContext())!!
        )
        currentFactory.setUpstreamDataSourceFactory(dataSourceFactory)

        return HlsMediaSource
            .Factory(currentFactory)
            .createMediaSource(MediaItem.fromUri(sourceUrI))
    }

    private fun registerPreferenceUpdateHandler() {
        preferenceChangeListener =  SharedPreferences.OnSharedPreferenceChangeListener() { preference, key ->
            run {
                onSharedPreferenceChange(preference, key)
            }
        }
        sharedPreferences!!.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    private fun destroyPreferenceUpdateHandler() {
        sharedPreferences!!.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    private fun onSharedPreferenceChange(preference: SharedPreferences?, key: String?)
    {
        if(key == SHOULD_LOOP_PREF_KEY)
        {
            val shouldLoop = preference!!.getBoolean(SHOULD_LOOP_PREF_KEY, true)
            currentPlayer!!.repeatMode = if(shouldLoop) Player.REPEAT_MODE_ONE else Player.REPEAT_MODE_OFF
        }
        else if(key == VIDEO_SOURCE_PREF_KEY)
        {
            val currentSourceURI = preference!!.getString(VIDEO_SOURCE_PREF_KEY, defaultSourceURI)!!
            currentPlayer!!.setMediaSource(getCacheBasedHlsMediaSource(currentSourceURI))
            currentPlayer!!.prepare()
            currentPlayer!!.play()
        }
    }

    override fun onPause() {
        super.onPause()
        currentPlayer!!.pause()
    }

    override fun onResume() {
        super.onResume()
        currentPlayer!!.play()
    }

    override fun onDestroy() {
        super.onDestroy()
        destroyPreferenceUpdateHandler()
    }


}


